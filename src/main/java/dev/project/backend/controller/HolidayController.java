package dev.project.backend.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import dev.project.backend.api.ErrorResponse;
import dev.project.backend.api.ResponseApi;
import dev.project.backend.constant.ApiConstant;
import dev.project.backend.dto.HolidayDTO;
import dev.project.backend.model.ApiLog;
import dev.project.backend.repository.ApiLogRepository;
import dev.project.backend.utils.HttpUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Controller
@RequestMapping(ApiConstant.API_HOLIDAY)
@RequiredArgsConstructor
public class HolidayController {

    private final ApiLogRepository apiLogRepository;

    @GetMapping
    public String getHoliday(@RequestParam(required = false) String country,
                             @RequestParam(required = false) Integer year,
                             @RequestParam(required = false) Integer month,
                             HttpServletRequest request,
                             HttpServletResponse response,
                             Model model) {
        String defaultCountry = request.getLocale().getCountry();
        int defaultYear = Calendar.getInstance().get(Calendar.YEAR);

        if (country == null || country.isEmpty()) {
            country = defaultCountry;
        }
        if (year == null) {
            year = defaultYear;
        }

        String apiUrl = "https://date.nager.at/api/v3/PublicHolidays/" + year + "/" + country;
        ResponseEntity<Object> apiResponse = getObjectResponseEntity(apiUrl, month, request, response);


        ObjectMapper objectMapper = new ObjectMapper();
        ResponseApi responseApi = (ResponseApi) apiResponse.getBody();
        List<HolidayDTO> holidays = objectMapper.convertValue(responseApi.getData(), TypeFactory.defaultInstance().constructCollectionType(List.class, HolidayDTO.class));

        model.addAttribute("holidays", holidays);

        System.out.println(model);

        return "index";
    }

    private ResponseEntity<Object> getObjectResponseEntity(String apiUrl, Integer month, HttpServletRequest request, HttpServletResponse response) {
        RestTemplate restTemplate = new RestTemplate();
        try {
            ResponseEntity<String> res = restTemplate.getForEntity(apiUrl, String.class);
            String json = res.getBody();
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode rootNode = objectMapper.readTree(json);
            List<JsonNode> filteredNodes = new ArrayList<>();
            for (JsonNode node : rootNode) {
                if (month != null) {
                    String date = node.get("date").asText();
                    int nodeMonth = Integer.parseInt(date.substring(5, 7));
                    if (nodeMonth == month) {
                        filteredNodes.add(node);
                    }
                } else {
                    filteredNodes.add(node);
                }
            }
            String ip = HttpUtils.getRequestIP(request);

            ApiLog apiLog = new ApiLog()
                    .setClientIp(ip)
                    .setRequestPath(request.getRequestURI())
                    .setRequestMethod(request.getMethod())
                    .setRequestParameter(request.getQueryString())
                    .setResponseStatus(response.getStatus());

            apiLogRepository.save(apiLog);

            return ResponseEntity.status(HttpStatus.OK)
                    .body(ResponseApi.success("success", 200, "Data retrieved successfully", filteredNodes));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ErrorResponse("error", HttpStatus.BAD_REQUEST.value(), e.getMessage()));
        }
    }
}