package dev.project.backend.model;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Data
@Accessors(chain = true)
@Entity
@Table(name = "API_LOG")
public class ApiLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "client_ip")
    private String clientIp;
    @Column(name = "request_path")
    private String requestPath;
    @Column(name = "request_method")
    private String requestMethod;
    @Column(name = "request_parameter")
    private String requestParameter;
    @Column(name = "request_status")
    private int responseStatus;
}