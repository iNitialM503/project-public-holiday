package dev.project.backend.dto;

import lombok.Data;

import java.util.List;

@Data
public class HolidayDTO {
    public String date;
    public String localName;
    public String name;
    public String countryCode;
    public boolean fixed;
    public boolean global;
    public List<String> counties;
    public Integer launchYear;
    public List<String> types;
}
