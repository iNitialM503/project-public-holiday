package dev.project.backend.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
public class ApiLogDTO {
    @JsonProperty("client_ip")
    private String clientIp;
    @JsonProperty("request_path")
    private String requestPath;
    @JsonProperty("request_method")
    private String requestMethod;
    @JsonProperty("request_parameter")
    private String requestParameter;
    @JsonProperty("request_status")
    private int responseStatus;
}