package dev.project.backend.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseApi {
    private String status;
    private int code;
    private String message;
    private Object data;

    public static ResponseApi success(String status, int code, String message, Object data){
        return new ResponseApi(status, code, message, data);
    }

}
