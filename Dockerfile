# Menggunakan base image dengan JDK 17 dan Maven
FROM maven:3.8.4-openjdk-17-slim AS builder

# Menyalin file project ke dalam image
COPY . /backend
WORKDIR /backend

# Mengkompilasi project menggunakan Maven
RUN mvn clean package -DskipTests

# Menggunakan base image dengan JRE 17
FROM --platform=linux/amd64 openjdk:17-alpine

# Menyalin artifact hasil build dari builder stage ke dalam image
COPY --from=builder /backend/target/*.jar /backend/backend.jar

EXPOSE 8010

# Menjalankan aplikasi ketika container dijalankan
CMD ["java", "-jar", "/backend/backend.jar"]